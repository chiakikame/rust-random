extern crate clap;
extern crate rand;

use clap::App;
use clap::Arg;
use rand::thread_rng;
use rand::Rng;

fn main() {
    let matches = App::new("random")
        .arg(
            Arg::with_name("start")
                .short("s")
                .long("start")
                .default_value("0")
                .validator(|v| {
                    i64::from_str_radix(&v, 10)
                        .map(|_| ())
                        .map_err(|_| "Expected integer for start".to_owned())
                })
                .help("Start of range, inclusive")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("end")
                .short("e")
                .long("end")
                .default_value("10")
                .validator(|v| {
                    i64::from_str_radix(&v, 10)
                        .map(|_| ())
                        .map_err(|_| "Expected integer for end".to_owned())
                })
                .help("End of range, EXCLUSIVE")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("count")
                .short("c")
                .long("count")
                .default_value("1")
                .validator(|v| {
                    u32::from_str_radix(&v, 10)
                        .map_err(|_| "Expected positive integer for count".to_owned())
                        .and_then(|v| {
                            if v > 0 {
                                Ok(())
                            } else {
                                Err("Expected positive integer for count".to_owned())
                            }
                        })
                })
                .help("# of generated numbers")
                .takes_value(true),
        )
        .get_matches();

    let start = matches
        .value_of("start")
        .and_then(|v| i64::from_str_radix(&v, 10).ok())
        .unwrap();
    let end = matches
        .value_of("end")
        .and_then(|v| i64::from_str_radix(&v, 10).ok())
        .unwrap();
    let count = matches
        .value_of("count")
        .and_then(|v| u32::from_str_radix(&v, 10).ok())
        .unwrap();

    for _ in 0..count {
        print_rnd(start, end);
    }
}

fn print_rnd(start: i64, end: i64) {
    let rand_value = if start < end {
        thread_rng().gen_range(start, end)
    } else if start > end {
        thread_rng().gen_range(end, start)
    } else {
        start
    };

    println!("{}", rand_value);
}
